#Nhom5

import os, sys
import time
import urllib.request
import json
import datetime
import requests
from pymongo import MongoClient

def btceGetTicker(pairs):

    try:
        ticker_raw = urllib.request.urlopen("https://tradeogre.com/api/v1/ticker/" + pairs)
        ticker_encoding = ticker_raw.info().get_content_charset('utf-8')
        ticker_json = json.loads(ticker_raw.read().decode(ticker_encoding))
        return ticker_json
    except Exception as e:
        print("Loi ngoai le", e)
    return None

def main():
    ACM = btceGetTicker("BTC-ACM")
    AEON = btceGetTicker("BTC-AEON")
    ARQ = btceGetTicker("BTC-ARQ")
    BBS = btceGetTicker("BTC-BBS")
    BCN = btceGetTicker("BTC-BCN")
    BEAM = btceGetTicker("BTC-BEAM")
    BKC = btceGetTicker("BTC-BKC")
    BLOC = btceGetTicker("BTC-BLOC")
    BSM = btceGetTicker("BTC-BSM")
    BTCP = btceGetTicker("BTC-BTCP")
    CCX = btceGetTicker("BTC-CCX")
    CIV = btceGetTicker("BTC-CIV")
    COAL = btceGetTicker("BTC-COAL")
    D = btceGetTicker("BTC-D")
    DASH = btceGetTicker("BTC-DASH")
    DERO = btceGetTicker("BTC-DERO")
    DOGE = btceGetTicker("BTC-DOGE")
    ETH = btceGetTicker("BTC-ETH")
    ETNX = btceGetTicker("BTC-ETNX")
    ETNXP = btceGetTicker("BTC-ETNXP")
    FBF = btceGetTicker("BTC-FBF")
    GPKR = btceGetTicker("BTC-GPKR")
    GRFT = btceGetTicker("BTC-GRFT")
    GRIN = btceGetTicker("BTC-GRIN")
    INC = btceGetTicker("BTC-INC")
    INTU = btceGetTicker("BTC-INTU")
    IRD = btceGetTicker("BTC-IRD")
    KRB = btceGetTicker("BTC-KRB")
    LNS = btceGetTicker("BTC-LNS")
    LOKI = btceGetTicker("BTC-LOKI")
    LTC = btceGetTicker("BTC-LTC")
    dbClientConnection = MongoClient('localhost', 27017)
    db = dbClientConnection['Nhom5']
    entry = db["ACM"]
    entry.insert_one(ACM)
    entry = db["AEON"]
    entry.insert_one(AEON)
    entry = db["ARQ"]
    entry.insert_one(ARQ)
    entry = db["BBS"]
    entry.insert_one(BBS)
    entry = db["BCN"]
    entry.insert_one(BCN)
    entry = db["BEAM"]
    entry.insert_one(BEAM)
    entry = db["BKC"]
    entry.insert_one(BKC)
    entry = db["BLOC"]
    entry.insert_one(BLOC)
    entry = db["BSM"]
    entry.insert_one(BSM)
    entry = db["BTCP"]
    entry.insert_one(BTCP)
    entry = db["CCX"]
    entry.insert_one(CCX)
    entry = db["CIV"]
    entry.insert_one(CIV)
    entry = db["D"]
    entry.insert_one(D)
    entry = db["DASH"]
    entry.insert_one(DASH)
    entry = db["DERO"]
    entry.insert_one(DERO)
    entry = db["DOGE"]
    entry.insert_one(DOGE)
    entry = db["ETH"]
    entry.insert_one(ETH)
    entry = db["ETNX"]
    entry.insert_one(ETNX)
    entry = db["ETNXP"]
    entry.insert_one(ETNXP)
    entry = db["FBF"]
    entry.insert_one(FBF)
    entry = db["GPKR"]
    entry.insert_one(GPKR)
    entry = db["GRFT"]
    entry.insert_one(GRFT)
    entry = db["INC"]
    entry.insert_one(INC)
    entry = db["INTU"]
    entry.insert_one(INTU)
    entry = db["IRD"]
    entry.insert_one(IRD)
    entry = db["KRB"]
    entry.insert_one(KRB)
    entry = db["LNS"]
    entry.insert_one(LNS)
    entry = db["LOKI"]
    entry.insert_one(LOKI)
    entry = db["LTC"]
    entry.insert_one(LTC)

if __name__ == '__main__':
    main()
